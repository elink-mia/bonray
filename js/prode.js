$(function() {
    $('section.pro:last').css({
        border: 'none'
    });
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: false,
        focusOnSelect: true
    });
    tabs();
    $('.slider-pro').slick({
        dots: true,
        arrows: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        lazyLoad: 'ondemand',
        adaptiveHeight: true,
        responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    $(window).resize(function() {
        dec_height();
    });
});
var dec_height = function() {
    var content = $('#page').height(),
        win_h = $(window).height(),
        main_h = $('#main').height(),
        need_h = win_h - content;
    if (content < win_h) {
        $('#main').css("min-height", main_h + need_h);
    }
};
var tabs = function() {
    $("#tabs .nav-item").click(function() {
        $(this).toggleClass('active', 5000);
    });
};