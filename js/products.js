$(function() {
    $('section.pro:last').css({
        border: 'none'
    });
    $('.slider-pro').slick({
        dots: true,
        arrows: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        lazyLoad: 'ondemand',
        adaptiveHeight: true,
        responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    ellipsis();
    $(window).resize(function() {
        dec_height();
    });
});
var dec_height = function() {
    var content = $('#page').height(),
        win_h = $(window).height(),
        main_h = $('#main').height(),
        need_h = win_h - content;
    if (content < win_h) {
        $('#main').css("min-height", main_h + need_h);
    }
};
var ellipsis = function() {
    var win_w = $(window).width();
    if (win_w > 768) {
        $('.ellipsis').ellipsis({
            row: 1
        });
    } else {
        $('.ellipsis').ellipsis({
            row: 2
        });
    }
};