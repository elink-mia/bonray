$(function() {
    ellipsis();
    icon();
	box();
    $(window).resize(function() {
        ellipsis();
        icon();
        box();
    });
});
var ellipsis = function() {
    $('.ellipsis').ellipsis({
        row: 2
    });
};
var box = function() {
    var win_w = $(window).width(),
        box_w = $('.know .know-item').width(),
        box = $('.know .know-item');
    if (win_w < 1680) {
        $(box).height(box_w);
    }
};
var icon = function() {
    var ic_w = $('.know .know-item .icon').width(),
        ic = $('.know .know-item .icon');
    $(ic).height(ic_w);
};