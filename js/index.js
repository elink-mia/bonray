$(function() {
    $('.slider-banner').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        lazyLoad: 'ondemand',
        dots: true,
        arrows: false,
        infinite: true,
        speed: 1000,
        fade: true,
        cssEase: 'linear'
    });
    $('.slider-brand').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        infinite: true,
        lazyLoad: 'ondemand',
        speed: 500,
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    centerMode: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    var box_w = $('#feature .box').width(),
        box = $('#feature .box');
    $(box).height(box_w);
    $(window).resize(function() {
        $(box).height(box_w);
    });
    $('.ellipsis2').ellipsis({
        row: 2
    });
});