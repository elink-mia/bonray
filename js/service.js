$(function() {
    $(".lazyload").lazyload();
    $('section.pro:last').css({
        border: 'none'
    });
    $('.slider-pro').slick({
        dots: true,
        arrows: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        adaptiveHeight: true,
        lazyLoad: 'ondemand',
        responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    ellipsis();
});
var ellipsis = function() {
    var win_w = $(window).width();
    if (win_w > 768) {
        $('.ellipsis').ellipsis({
            row: 1
        });
    } else {
        $('.ellipsis').ellipsis({
            row: 2
        });
    }
};