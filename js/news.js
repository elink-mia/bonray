$(function() {
    $(".lazyload").lazyload();
    ellipsis_news();
    ellipsis_know();
    $(window).resize(function() {
        ellipsis_news();
        ellipsis_know();
    });
});
/* news */
var ellipsis_news = function() {
    var win_w = $(window).width();
        $('.newsss .ellipsis1').ellipsis({
            row: 1
        });
        $('.newsss .ellipsis2').ellipsis({
            row: 2
        });
    if (win_w > 992) {
        $('.first .ellipsis2').ellipsis({
            row: 2
        });
        $('.first .ellipsis4').ellipsis({
            row: 4
        });
    } else if (win_w < 991 && win_w > 769) {
        $('.first .ellipsis2').ellipsis({
            row: 2
        });
        $('.first .ellipsis4').ellipsis({
            row: 2
        });
    } else {
        $('.first .ellipsis2').ellipsis({
            row: 1
        });
        $('.first .ellipsis4').ellipsis({
            row: 2
        });
    }
};
/* knowleage */
var ellipsis_know = function() {
    var win_w = $(window).width();
    if (win_w > 992) {
        $('.know-de .ellipsis1').ellipsis({
            row: 1
        });
    	$('.know-de .ellipsis2').ellipsis({
        	row: 9
    	});
    } else if (win_w < 991 && win_w > 769) {
        $('.know-de .ellipsis1').ellipsis({
            row: 1
        });
    	$('.know-de .ellipsis2').ellipsis({
        	row: 3
    	});
    } else {
        $('.know-de .ellipsis1, .know-de .ellipsis2').ellipsis({
            row: 2
        });
    }
};